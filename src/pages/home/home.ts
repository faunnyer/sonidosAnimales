import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { ANIMALES } from '../../data.animales';
import { Animal } from '../../interfaces/animal.interface';
import { Observable } from '../../../node_modules/rxjs/Observable';
import { timer } from '../../../node_modules/rxjs/observable/timer';
import { ModalPage } from '../modal/modal';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  animales: Animal[] = [];

  constructor(public navCtrl: NavController,public modalCtrl: ModalController) {

    this.animales = ANIMALES.slice(0);
    console.log(this.animales);

  }

  reproducir(animal: Animal) {

      const modal = this.modalCtrl.create(ModalPage, { 'animal': animal , 'animales': this.animales });
      modal.onDidDismiss(item => {
        if (item) {
        
        }
      });
      modal.present();
      
  }

  borrar(idx) {

    this.animales.splice(idx, 1);

  }

  doRefresh(refresher) {
    console.log('Refrescando', refresher);

    setTimeout(() => {
      console.log('Termima el refresh');
      this.animales = ANIMALES.slice(0);
      refresher.complete();
    }, 2000);

  }



}
