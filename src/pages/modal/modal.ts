import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Animal } from '../../interfaces/animal.interface';
import { timer } from '../../../node_modules/rxjs/observable/timer';
import { take, timeInterval } from '../../../node_modules/rxjs/operators';

@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  animalView:Animal;
  animalesView:Animal[];

  audio = new Audio();
  audioTiempo: any;
  valor = 0;


  constructor(public navCtrl: NavController, public navParams: NavParams,   public viewCtrl: ViewController, ) {

    if (this.navParams.get('animal')) {
      this.animalView = this.navParams.get('animal');
      this.animalesView = this.navParams.get('animal');
      this.reproducir(this.animalView);
    }

  }


  reproducir(animalView: Animal) {

    this.valor = 0;

    this.pausarAudio(this.animalView);

    if (this.animalView.reproduciendo) {
      this.animalView.reproduciendo = false;
      return
    }

    this.audio.src = this.animalView.audio;
    this.audio.load();
    this.audio.play();
    this.barraEstado(this.animalView.duracion);

    this.animalView.reproduciendo = true;
    this.audioTiempo = setTimeout(() => this.animalView.reproduciendo = false, this.animalView.duracion * 1000);

  }

  barraEstado(tiempoTotal) {
    const playSong = timer(0, 5).pipe(  
                      timeInterval(),
                      take((tiempoTotal * 80))
                    );
    playSong.subscribe(val => {
      this.mueveLineaReproductor(tiempoTotal);
    });
  }

  mueveLineaReproductor(valorMaximo){
    this.valor = this.valor + 0.25;
  }

  pausarAudio(animalSeleccionado: Animal) {

    clearTimeout(this.audioTiempo);
    this.audio.pause();
    this.audio.currentTime = 0;

    for (let animal of this.animalesView) {

      if (animal.nombre != animalSeleccionado.nombre) {
        animal.reproduciendo = false;
      }

    }

  }


  cancel() {
    this.audio.pause();
    this.audio.currentTime = 0;
    this.viewCtrl.dismiss();
  }




}
